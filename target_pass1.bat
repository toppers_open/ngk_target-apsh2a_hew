@echo off
rem コンフィギュレーション pass1を実行
rem 　　ターゲット名を引数に追加して
rem 　　プロセッサ依存部のprc_pass1.batを呼び出す。

rem 引数
rem 　%1 aspのトップディレクトリ
rem 　%2 対象となるコンフィギュレーションファイル

@echo on
call %1\arch\sh12a_hew\prc_pass1.bat %1 %2 apsh2a_hew
