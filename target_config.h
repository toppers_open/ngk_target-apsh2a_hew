/*
 *  TOPPERS/ASP Kernel
 *      Toyohashi Open Platform for Embedded Real-Time Systems/
 *      Advanced Standard Profile Kernel
 * 
 *  Copyright (C) 2007 by Embedded and Real-Time Systems Laboratory
 *              Graduate School of Information Science, Nagoya Univ., JAPAN
 * 
 *  上記著作権者は，以下の(1)〜(4)の条件を満たす場合に限り，本ソフトウェ
 *  ア（本ソフトウェアを改変したものを含む．以下同じ）を使用・複製・改
 *  変・再配布（以下，利用と呼ぶ）することを無償で許諾する．
 *  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
 *      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
 *      スコード中に含まれていること．
 *  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
 *      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
 *      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
 *      の無保証規定を掲載すること．
 *  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
 *      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
 *      と．
 *    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
 *        作権表示，この利用条件および下記の無保証規定を掲載すること．
 *    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
 *        報告すること．
 *  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
 *      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
 *      また，本ソフトウェアのユーザまたはエンドユーザからのいかなる理
 *      由に基づく請求からも，上記著作権者およびTOPPERSプロジェクトを
 *      免責すること．
 * 
 *  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
 *  よびTOPPERSプロジェクトは，本ソフトウェアに関して，特定の使用目的
 *  に対する適合性も含めて，いかなる保証も行わない．また，本ソフトウェ
 *  アの利用により直接的または間接的に生じたいかなる損害に関しても，そ
 *  の責任を負わない．
 * 
 *  @(#) $Id: target_config.h 696 2007-12-25 04:42:33Z honda $
 */

/*
 *  ターゲット依存モジュール（APSH2A用）
 *
 *  カーネルのターゲット依存部のインクルードファイル．kernel_impl.hのター
 *  ゲット依存部の位置付けとなる．
 */

#ifndef TOPPERS_TARGET_CONFIG_H
#define TOPPERS_TARGET_CONFIG_H

/*
 *  ターゲットシステムのハードウェア資源の定義
 */
#include "apsh2a.h"

/*
 *  トレースログに関する設定
 */
#ifdef TOPPERS_ENABLE_TRACE
#include "logtrace\trace_config.h"
#endif /* TOPPERS_ENABLE_TRACE */

/*
 *  ターゲット依存の文字出力に使用するポートの定義  
 */
#define TARGET_PUTC_PORTID 2

/*
 *  ASPカーネル動作時のメモリマップと関連する定義
 */

/*
 *  デフォルトの非タスクコンテキスト用のスタック領域の定義
 */
#define DEFAULT_ISTKSZ    0x2000U

#define DEFAULT_ISTK      (void *)(DEFAULT_STK_TOP - DEFAULT_ISTKSZ)

#ifndef TOPPERS_MACRO_ONLY

/*
 *  ターゲットシステム依存の初期化
 */
extern void target_initialize(void);

/*
 *  ターゲットシステムの終了
 *
 *  システムを終了する時に使う．
 */
extern void target_exit(void);

#endif /* TOPPERS_MACRO_ONLY */

/*
 *  微少時間待ちのための定義（本来はSILのターゲット依存部）
 *  
 *  注意　この記述はテストプログラムasp/test/test_dlynse.c用であり、
 *  　　　このマクロを修正しても、実際のsil_dly_nseの動作は変わらない。
 *  　　　カーネル内部では、target_support.incにあるアセンブラ用
 *  　　　マクロ定義を用いているので、こちらも修正すること。
 */
#ifdef ROM_BOOT		/*  ROM化の場合  */
#define SIL_DLY_TIM1     75
#define SIL_DLY_TIM2     40

#else				/*  RAM上でデバッグする場合  */
#define SIL_DLY_TIM1     2020
#define SIL_DLY_TIM2      709

#endif

/*
 *  プロセッサ依存モジュール（SH7211用）
 */
#include "sh12a_hew\sh7211_config.h"
     
#endif  /* TOPPERS_TARGET_CONFIG_H */
